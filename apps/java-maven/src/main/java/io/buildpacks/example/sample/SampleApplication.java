package io.buildpacks.example.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
	}

}

// help me create a public method that prints any application logs from SampleApplication to console


@Component
public class SampleApplication {

  private static final Logger LOG = LoggerFactory.getLogger(SampleApplication.class);

  public void printLogs() {
    LOG.info("Printing application logs");
    LOG.debug("Here is a debug message");
    LOG.warn("Here is a warning");
    LOG.error("Something went wrong!");
  }

}
public static void printLogs() {
    Logger logger = LoggerFactory.getLogger(SampleApplication.class);
    logger.info("Application logs from SampleApplication");
}


@Test
public void testMain() {
    SampleApplication.main(new String[0]);
    
    // Assertions to check that the Spring application started correctly
    // could go here, e.g.:
    assertTrue(context.containsBean("someSpringBean"));
}

@Test 
public void testSampleApplication() {
    assertNotNull(new SampleApplication()); 
}

@SpringBootTest
public class SampleApplicationTests {

    @Autowired
    private SampleApplication app;
    
    @Test
    public void contextLoads() {
        assertThat(app).isNotNull();
    }
}